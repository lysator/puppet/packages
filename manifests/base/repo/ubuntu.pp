class packages::base::repo::ubuntu (
  String $ftp_host = 'ftp.lysator.liu.se',
  String $ftp_proto = 'https',
  Array[String] $repo_list  = [ 'main', 'restricted', 'universe' ],
) {

  $repos = join($repo_list, ' ')
  $dist = $facts['os']['distro']['codename']

  class { '::apt':
    purge => {
      'sources.list'   => true,
      'sources.list.d' => true,
      'preferences'    => true,
      'preferences.d'  => true,
    }
  }
  -> apt::source { $dist:
    location => "${ftp_proto}://${ftp_host}/ubuntu/",
    release  => $dist,
    repos    => $repos,
  }
  -> apt::source { "${dist}-security":
    location => "${ftp_proto}://${ftp_host}/ubuntu/",
    repos    => $repos,
    release  => "${dist}-security"
  }
  -> apt::source { "${dist}-updates":
    location => "${ftp_proto}://${ftp_host}/ubuntu/",
    repos    => $repos,
    release  => "${dist}-updates"
  }
  -> apt::source { "${dist}-backports":
    location => "${ftp_proto}://${ftp_host}/ubuntu/",
    repos    => $repos,
    release  => "${dist}-backports"
  }
}
