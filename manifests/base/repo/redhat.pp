class packages::base::repo::redhat {
  package { 'epel-release':
    ensure => installed,
  }

  file { '/etc/yum.repos.d/prometheus.repo':
    source => 'puppet:///modules/packages/prometheus.repo',
  }
}
