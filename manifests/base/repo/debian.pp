class packages::base::repo::debian (
  String $ftp_host = 'ftp.lysator.liu.se',
  String $ftp_proto = 'https',
) {
  $repo_list = if versioncmp(fact('os.release.major'), '12') >= 0 {
    [ 'main', 'contrib', 'non-free', 'non-free-firmware' ]
  } else {
    [ 'main', 'contrib', 'non-free' ]
  }
  $repos = join($repo_list, ' ')
  $dist = $facts['os']['distro']['codename']

  class { '::apt':
    purge => {
      'sources.list'   => true,
      'sources.list.d' => true,
      'preferences'    => true,
      'preferences.d'  => true,
    }
  }

  apt::source { $dist:
    location => "${ftp_proto}://${ftp_host}/debian/",
    release  => $dist,
    repos    => $repos,
    require  => Class['apt'],
  }

  if (Integer($::facts['os']['release']['major']) >= 11) {
    apt::source { "${dist}-security":
      location => "${ftp_proto}://${ftp_host}/debian-security/",
      repos    => $repos,
      release  => "${dist}-security",
      require  => Class['apt'],
    }
  } else {
    apt::source { "${dist}-security":
      location => "${ftp_proto}://${ftp_host}/debian-security/",
      repos    => $repos,
      release  => "${dist}/updates",
      require  => Class['apt'],
    }
  }

  apt::source { "${dist}-updates":
    location => "${ftp_proto}://${ftp_host}/debian/",
    repos    => $repos,
    release  => "${dist}-updates",
    require  => Class['apt'],
  }
  apt::source { "${dist}-backports":
    location => "${ftp_proto}://${ftp_host}/debian/",
    repos    => $repos,
    release  => "${dist}-backports",
    require  => Class['apt'],
  }
}
