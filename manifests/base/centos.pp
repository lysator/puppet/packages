class packages::base::centos {
  ensure_packages ([
    'bind-utils',
    'vim-enhanced',
  ], {
    ensure => installed,
  })
}
