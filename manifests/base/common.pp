class packages::base::common {
  ensure_packages([
    'bash-completion',
    'fping',
    'git',
    'htop',
    'iftop',
    'iotop',
    'lshw',
    'lsof',
    'pciutils',
    'rsync',
    'screen',
    'strace',
    'sysstat',
    'tcsh',
    'telnet',
    'tmux',
    'traceroute',
    'usbutils',
    'wget',
    'zsh',
  ], {
    ensure => installed,
  })
}
