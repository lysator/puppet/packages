class packages::base::redhat {
  ensure_packages ([
    'bind-utils',
    'vim-enhanced',
  ], {
    ensure => installed,
  })
}
