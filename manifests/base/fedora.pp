class packages::base::fedora {
  ensure_packages([
    'redhat-lsb',
    'bind-utils',
    'vim-enhanced',
  ], {
    ensure => installed,
  })
}
