class packages::base::rocky {
  ensure_packages([
    'bind-utils',
    'vim-enhanced',
  ], {
    ensure => installed,
  })

  if versioncmp(fact('os.release.major'), '9') < 0 {
    ensure_packages(['redhat-lsb'])
  }
}
