# Base packages, automatically selects apropriate defaults depending
# on the os.
class packages::base {
  require "::packages::base::repo::${facts['os']['name']}"
  include ::packages::base::common
  include "packages::base::${facts['os']['name']}"
}
